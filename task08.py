# Find all types of transportation modes and count how many distinct users that
# have used the different transportation modes. Do not count the rows where the
# transportation mode is null.

from helper import make_query
from tabulate import tabulate

query = """
        SELECT Ac.transportation_mode, count(Ac.user_id)
            FROM (SELECT DISTINCT user_id, transportation_mode
                FROM ex2_db_test.Activity
                WHERE transportation_mode IS NOT NULL
            ) AS Ac
            GROUP BY Ac.transportation_mode;
        """
rows, cursor = make_query(query)
print(tabulate(rows, headers=cursor.column_names))


