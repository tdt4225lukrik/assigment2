# Find activities that are registered multiple times. You should find the query
# even if you get zero results.

from helper import make_query
from tabulate import tabulate


# Search for duplicate records with different IDs
# Counts the rows grouped by all attributes other than PK.
# Returns the rows that have count > 1.
query = """
        SELECT user_id, start_date, end_date, transportation_mode, COUNT(*)
        FROM Activity
        GROUP BY user_id, start_date, end_date, transportation_mode
        HAVING COUNT(*)>1
        """
rows, cursor = make_query(query)
print(tabulate(rows, headers=cursor.column_names))