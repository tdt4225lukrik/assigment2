class User:

    def __init__(self, id, has_labels):
        self.id = id
        self.has_labels = has_labels


class Activity:

    def __init__(self, id, user_id, transportation_mode, start_date_time, end_date_time):
        self.id = id
        self.user_id = user_id
        self.transportation_mode = transportation_mode
        self.start_date_time = start_date_time
        self.end_date_time = end_date_time


class TrackPoint:

    def __init__(self, id, activity_id, lat, lon, altitude, date_days, date_time):
        self.id = id
        self.activity_id = activity_id
        self.lat = lat
        self.lon = lon
        self.altitude = altitude
        self.date_days = date_days
        self.date_time = date_time


class Label:

    def __init__(self, start_date_time, end_date_time, mode):
        self.start_date_time = start_date_time
        self.end_date_time = end_date_time
        self.mode = mode

