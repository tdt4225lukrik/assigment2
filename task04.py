# Find the number of users that have started the activity in one day and ended
# the activity the next day.
from helper import make_query
from tabulate import tabulate

query = """
        SELECT COUNT(DISTINCT user_id) 
            FROM ex2_db_test.Activity
            WHERE DAY(end_date) = DAY(start_date) + 1;
        """
rows, cursor = make_query(query)
print(tabulate(rows, headers=cursor.column_names))