# Find the total distance (in km) walked in 2008, by user with id=112.

from helper import make_query
from tabulate import tabulate
from haversine import haversine, Unit

query = """
        SELECT * 
            FROM (SELECT *
                FROM ex2_db_test.Activity
                WHERE user_id = 112 AND transportation_mode = 'walk' AND YEAR(start_date) = 2008
            ) AS Ac
            INNER JOIN ex2_db_test.TrackPoint AS Tp ON Ac.id = Tp.activity_id
            ORDER BY Ac.id, Tp.id;
        """
rows, cursor = make_query(query)
# print(tabulate(rows, headers=cursor.column_names))

activity_id = 0
total_distance = 0
for i in range(1, len(rows)):
    row = rows[i]
    if activity_id != row[0]:
        activity_id = row[0]
        continue
    last_row = rows[i - 1]
    last_pos = (last_row[6], last_row[7])
    pos = (row[6], row[7])
    distance = haversine(last_pos, pos, unit=Unit.METERS)
    # print(distance)
    total_distance += distance
print(total_distance)

