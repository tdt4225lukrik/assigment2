# a) Find the year and month with the most activities.
# b) Which user had the most activities this year and month, and how many
# recorded hours do they have? Do they have more hours recorded than the user
# with the second most activities?

from helper import make_query
from tabulate import tabulate

# Task a)
# The single month with the most activities RESULT: Year, Month, ActivityCount: 2008, 11, 1006
query_a = """
        SELECT
                YEAR(start_date) AS Year,
                MONTH(start_date) AS Month,
                COUNT(*) AS ActivityCount
        FROM Activity
        GROUP BY Year, Month
        ORDER BY ActivityCount DESC
        LIMIT 1;
        """

# Task b)
# Users with most activities in November 2008 and their recorded hours.
query_b = """
        SELECT User_ID, COUNT(*) AS ActivityCount,
        HOUR(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(end_date, start_date))))) AS Hours
        FROM Activity
        WHERE YEAR(start_date) = 2008 AND MONTH(start_date) = 11
        GROUP BY user_id
        ORDER BY ActivityCount DESC;
        """

rows_a, cursor_a = make_query(query_a)
rows_b, cursor_b = make_query(query_b)
print(tabulate(rows_a, headers=cursor_a.column_names))
print() # Empty line
print(tabulate(rows_b, headers=cursor_b.column_names))