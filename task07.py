# Find all users that have never taken a taxi.

from helper import make_query
from tabulate import tabulate

# Find the difference between all user IDS and the user IDS that have taken a taxi.
query = """
        SELECT id
        FROM User       
        WHERE id NOT IN (
            SELECT DISTINCT(user_id)
            FROM Activity
            WHERE transportation_mode = 'taxi'
        )
        """
rows, cursor = make_query(query)
print(tabulate(rows, headers=cursor.column_names))