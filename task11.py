# Find the top 20 users who have gained the most altitude meters.
# ○ Output should be a table with (id, total meters gained per user).
# ○ Remember that some altitude-values are invalid
# ○ Tip: ∑(tp_ n.altitude − tp_n-1.altitude), tp_n.altitude > tp_n-1.altitude 

from helper import make_query
from tabulate import tabulate

# Dict for total altitude gained per user
user_altitude = {}

# Helper function for users that doesn't exist in dictionary yet
def add_to_user(user, value):
        if user not in user_altitude.keys():
                user_altitude[user] = value
        else:
                user_altitude[user] += value


# The id and user_id for all activities
activities, _ = make_query("SELECT id, user_id FROM Activity;")

# Loop through each activity
for activity in activities:
        # activity[0] = Activity.id
        # activity[1] = user_id
        print(activity[0])
        trackpoints, _ = make_query('SELECT date_time, altitude FROM TrackPoint WHERE activity_id = '+str(activity[0])+' ORDER BY date_time')
        
        # Sorts the list by date_time, but is already sorted with ORDER BY.
        # trackpoints.sort(key=lambda row:row[0])

        # Initial value
        activity_altitude = 0
        
        # For the second to the last trackpoint
        for i in range(1, len(trackpoints)):
                # Find the altitude difference from the previous checkpoint
                diff_altitude = trackpoints[i][1] - trackpoints[i-1][1]
                # If the altitude difference is positive i.e. height gained, add to activity total
                if diff_altitude > 0: activity_altitude += diff_altitude
        # Add the activity total to the user's total
        add_to_user(activity[1], activity_altitude)

user_altitude_sorted = list(sorted(user_altitude.items(), key=lambda item: item[1], reverse=True))
top_user_altitude = user_altitude_sorted[:20]

# print(tabulate(top_user_altitude, headers=["user_id, meters"]))

print(tabulate(top_user_altitude, headers=["User_id", "Total meters"]))