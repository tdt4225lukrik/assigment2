from models import User, Activity, TrackPoint, Label
from DbConnector import DbConnector
from datetime import datetime
import os

connection = DbConnector()
db_connection = connection.db_connection
cursor = connection.cursor


USER_COUNT = 182
def add_users():
    # Add all users
    users = []
    for i in range(USER_COUNT):
        users.append(User(id=str(i).rjust(3, "0"), has_labels=False))

    # Update everyone that have labels
    label_file = open("dataset\labeled_ids.txt")
    for line in label_file.readlines():
        # Find user with id of line
        user: User = list(filter(lambda user: user.id == line.strip(), users))[0] 
        user.has_labels = True
    label_file.close()

    values_string = ",".join(list(map(lambda u: "('" + u.id + "'," + str(u.has_labels) + ")", users)))

    query = """INSERT INTO User VALUES %s;""" % (values_string)
    cursor.execute(query)
    db_connection.commit()

    print(query)

    # print(list(map(lambda user: user.has_labels, users)))
    
    return users


def walk():
    dir_it = os.walk("dataset\Data")
    
    # Jump over root case
    next(dir_it)

    # Iterate through all User folders
    while dir_it:
        try:
            dirpath, dirnames, filenames = next(dir_it)
        except StopIteration:
            break
        
        # Get User Id
        user_id = dirpath[-3:]
        print(user_id)

        # Go through labels (labels.txt)
        labels = []
        if "labels.txt" in filenames:
            file = open(dirpath + "\\" + "labels.txt")
            lines = file.readlines()

            # Jump over header
            lines = lines[1:]

            for line in lines:
                row = line.strip().split("\t")
                # Convert times to datetime
                start_date_time = datetime.strptime(row[0], "%Y/%m/%d %H:%M:%S")
                end_date_time = datetime.strptime(row[1], "%Y/%m/%d %H:%M:%S")

                mode = row[2]
                label = Label(start_date_time, end_date_time, mode)
                labels.append(label)

            file.close()
        
        # Iterate through all files in Trajectory folder
        dirpath, dirnames, filenames = next(dir_it)
        activities = []
        for filename in filenames:
            file = open(dirpath + "\\" + filename)
            lines = file.readlines()
            
            # Do not include file if over 2500 lines
            if len(lines) >= 2500 + 6:
                continue
            
            # Remove 6 first lines
            lines = lines[6:]

            # Create Activity
            start_row = lines[0].strip().split(",")
            end_row = lines[-1].strip().split(",")
            start_date_time = datetime.strptime(start_row[5] + " " + start_row[6], "%Y-%m-%d %H:%M:%S")
            end_date_time = datetime.strptime(end_row[5] + " " + end_row[6], "%Y-%m-%d %H:%M:%S")
            activity = Activity(None, user_id, None, start_date_time, end_date_time)

            # Check if there are labels
            label_match = list(filter(lambda label: label.start_date_time == activity.start_date_time and label.end_date_time == activity.end_date_time, labels))
            if len(label_match) > 0:
                activity.transportation_mode = label_match[0].mode

            # print(activity.id, activity.user_id, activity.transportation_mode, activity.start_date_time, activity.end_date_time)

            # Insert activity in database
            query = """INSERT INTO 
                    Activity(id, transportation_mode, start_date, end_date, user_id)
                    VALUES (NULL, %s, '%s', '%s', '%s');
                    """ % ("'" + activity.transportation_mode + "'" if activity.transportation_mode else "NULL",
                            activity.start_date_time,
                            activity.end_date_time,
                            user_id)
            cursor.execute(query)
            db_connection.commit()
            activity_id = cursor.lastrowid

            # Go through lines of file and create list of TrackPoint
            track_points = []
            for line in lines:
                # Create TrackPoint
                row = line.strip().split(",")
                lat = float(row[0])
                lon = float(row[1])
                altitude = int(float(row[3]))
                date_time = datetime.strptime(row[5] + " " + row[6], "%Y-%m-%d %H:%M:%S")
                track_point = TrackPoint(None, activity_id, lat, lon, altitude, None, date_time)

                track_points.append(track_point)

                # print(line.strip())
            
            # Insert all trackpoints for this activity as Bulk insert
            # Transform array of all trackpoints into a string of tuples that can be used to insert with SQL
            values_string = ",".join(list(map(lambda tp: "(" + ",".join(["NULL", str(tp.lat), str(tp.lon), str(tp.altitude), "'" + str(tp.date_time) + "'", str(tp.activity_id)]) + ")", track_points)))
            query = """INSERT INTO 
                    TrackPoint(id, lat, lon, altitude, date_time, activity_id) 
                    VALUES %s;""" % (values_string)
            cursor.execute(query)
            db_connection.commit()
            # print(values_string)

            file.close()
        


# add_users()
walk()