# Find the number of users which have been close to each other in time and
# space (Covid-19 tracking). Close is defined as the same minute (60 seconds)
# and space (100 meters).
from DbConnector import DbConnector
from models import Activity, TrackPoint
from helper import make_query
from tabulate import tabulate
from haversine import haversine, Unit

connection = DbConnector()

# All the code below is helper functions used in the main part
# -------------------------------------------------------------------------------------------------

def rowToActivity(row):
    return Activity(row[0], row[4], row[1], row[2], row[3])

# Find if two activities overlap within interval of seconds (60)
def activity_time_overlap(activity1: Activity, activity2: Activity, interval):
    return activity1.start_date_time.timestamp() - interval <= activity2.end_date_time.timestamp() and activity1.end_date_time.timestamp() + interval >= activity2.start_date_time.timestamp()

# Find if two trackpoints have overlap in space based on treshold (will be 100 meters here)
def trackpoints_close(tp1: TrackPoint, tp2: TrackPoint, treshold):
    distance = haversine((tp1.lat, tp1.lon), (tp2.lat, tp2.lon), unit=Unit.METERS)
    return distance <= treshold

# Find if the trackpoints for two activities have overlap in space (100 meters) and are close in time (60s)
def activity_trackpoints_overlap(activity1: Activity, activity2: Activity):
    # Query to get all trackpoints for a specific activity_id
    activityTrackPointsQuery = """
    SELECT * FROM (SELECT *
            FROM ex2_db_test.Activity
            WHERE id = %s
    ) AS Ac
        INNER JOIN ex2_db_test.TrackPoint AS Tp ON Ac.id = Tp.activity_id
        ORDER BY Ac.id, Tp.id;
    """
    # Make the queries for the two activities
    tp1_rows, cursor1 = make_query(activityTrackPointsQuery % activity1.id, connection)
    tp2_rows, cursor2 = make_query(activityTrackPointsQuery % activity2.id, connection)
    
    # For each trackpoint in the one table go through each trackpoint in the other table and see if the trackpoints are close
    # If we find close trackpoints stop looping and return the result
    found = False
    for i in range(len(tp1_rows)):
        if found:
            break
        row_i = tp1_rows[i]
        tp1 = TrackPoint(row_i[5], row_i[0], row_i[6], row_i[7], row_i[8], None, row_i[9])
        for j in range(len(tp2_rows)):
            row_j = tp2_rows[j]
            tp2 = TrackPoint(row_j[5], row_j[0], row_j[6], row_j[7], row_j[8], None, row_j[9])
            if trackpoints_close(tp1, tp2, 100) and abs((tp1.date_time - tp2.date_time).total_seconds()) <= 60:
                print(row_i[6], row_i[7], row_i[9], row_j[6], row_j[7], row_j[9])
                found = True
                break
    return found

# ---------------------------------------------------------------------------------------------
# Here the main part begins
# Get all activitites
query = """
    SELECT * 
        FROM ex2_db_test.Activity
        ORDER BY user_id, id;
    """
rows, cursor = make_query(query, connection)


# Find all the activities that overlap in time
# so that we only have to check time and space overlap for the trackpoints in these activities
# Also skip activities that have user_ids that have already overlapped
overlaps = []
overlap_dict = {}
for i in range(len(rows)):
    activity_i = rowToActivity(rows[i])
    for j in range(i + 1, len(rows)):
        activity_j = rowToActivity(rows[j])
        
        # If the user_ids of the activities are equal skip this activity
        if activity_i.user_id == activity_j.user_id:
            continue

        dict_user_ids = activity_i.user_id + "-" + activity_j.user_id
        #  If the user_ids already have overlapped we skip this activity
        if dict_user_ids in overlap_dict:
            continue

        if activity_time_overlap(activity_i, activity_j, 60):
            # See if the trackpoints for the activites overlap in space and time
            if activity_trackpoints_overlap(activity_i, activity_j):
                overlaps.append((activity_i.user_id, activity_j.user_id))
                overlap_dict[dict_user_ids] = True
                print((activity_i.user_id, activity_j.user_id))
            


print(tabulate(overlaps, ["user_id", "user_id"]))
# print(tabulate(rows, headers=cursor.column_names))


#print(tabulate(rows, headers=cursor.column_names))

