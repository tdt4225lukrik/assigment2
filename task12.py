# Find all users who have invalid activities,
# and the number of invalid activities per user
# ○ An invalid activity is defined as an activity with consecutive trackpoints
# where the timestamps deviate with at least 5 minutes.

from helper import make_query
from DbConnector import DbConnector
from tabulate import tabulate

connection = DbConnector()

# Go through all users
invalid_users = set({})
results = []
for i in range(182):
    user_id = (str(i).rjust(3, "0"))
    # Find all trackpoints for the specific user, sorted in order of activity_id and trackpoint_id so subsequent rows can be compared
    query = """
            SELECT * 
                FROM (SELECT *
                    FROM ex2_db_test.Activity
                    WHERE user_id = %s
                ) AS Ac
                INNER JOIN ex2_db_test.TrackPoint AS Tp ON Ac.id = Tp.activity_id
                ORDER BY Ac.id, Tp.id;
            """ % user_id
    rows, cursor = make_query(query, connection)

    invalid_actitivites = 0
    found_invalid = False # if we have found a violation for the current activity
    # Go through all trackpoints and compare each subsequent trackpoint in time
    for j in range(1, len(rows)):
        last_row = rows[j - 1]
        current_row = rows[j]

        # If the two trackpoints come from different activities skip the comparison 
        if last_row[10] != current_row[10]:
            found_invalid = False
            continue
        
        # If we have already found a violation we skip the comparison
        # so we don't count the same activity multiple times 
        if found_invalid:
            continue

        # Do the time comparison
        last_time = last_row[9]
        current_time = current_row[9]
        diff = (current_time - last_time).total_seconds() / 60
        if diff >= 5:
            # print(last_time, current_time, diff)
            invalid_users.add(user_id)
            invalid_actitivites += 1
            found_invalid = True

    # Add to results
    if user_id in invalid_users:
        print((user_id, invalid_actitivites))
        results.append((user_id, invalid_actitivites))


#print(tabulate(rows, headers=cursor.column_names))
print(tabulate(results, headers=["user_id", "Invalid activities"]))
