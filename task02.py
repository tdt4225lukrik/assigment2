# Find the average, minimum and maximum number of activities per user.
from helper import make_query
from tabulate import tabulate

query = """
        SELECT MAX(Ac.Activities) AS "Max", AVG(Ac.Activities) AS "Average", MIN(Ac.Activities) AS "Minimum"
            FROM (SELECT user_id, COUNT(*) AS Activities
                FROM ex2_db_test.Activity
                GROUP BY user_id
            ) AS Ac
        """
rows, cursor = make_query(query)
print(tabulate(rows, headers=cursor.column_names))

