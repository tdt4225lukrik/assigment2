# Find the top 10 users with the highest number of activities.

from helper import make_query
from tabulate import tabulate

query = """
        SELECT User_ID, COUNT(id) AS ActivityCount
        FROM Activity
        GROUP BY user_id
        ORDER BY ActivityCount DESC
        LIMIT 10;
        """
rows, cursor = make_query(query)
print(tabulate(rows, headers=cursor.column_names))