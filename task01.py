# How many users, activities and trackpoints are there in the dataset (after it is
# inserted into the database).

from helper import make_query
from tabulate import tabulate

query = """
        SELECT
            (SELECT COUNT(id) FROM User) AS UserCount,
            (SELECT COUNT(id) FROM Activity) AS ActivityCount,
            (SELECT COUNT(id) FROM TrackPoint) AS TrackPointCount
        """
rows, cursor = make_query(query)
print(tabulate(rows, headers=cursor.column_names))