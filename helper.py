from DbConnector import DbConnector

def make_query(query, connection = None):
    if connection == None:
        connection = DbConnector()
    db_connection = connection.db_connection
    cursor = connection.cursor
    cursor.execute(query)
    rows = cursor.fetchall()
    return rows, cursor