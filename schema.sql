CREATE TABLE User(
	id VARCHAR(3) PRIMARY KEY,
    has_labels BOOLEAN
);

CREATE TABLE Activity(
	id INT AUTO_INCREMENT PRIMARY KEY,
	transportation_mode VARCHAR(10),
    start_date DATETIME,
    end_date DATETIME,
    user_id VARCHAR(3) NOT NULL,
    FOREIGN KEY (user_id) REFERENCES User(id) ON DELETE CASCADE
);

CREATE TABLE TrackPoint(
	id INT AUTO_INCREMENT PRIMARY KEY,
	lat DOUBLE,
    lon DOUBLE,
    altitude INT,
    date_time DATETIME,
    activity_id INT NOT NULL,
    FOREIGN KEY (activity_id) REFERENCES Activity(id) ON DELETE CASCADE
);